# gitlab-cli-reports Changelog


## 1.1.0 (2019-02-07)

- Add `--filter-by-project-membership` flag to make usage on large instances with many users (such as gitlab.com) viable.
- Add support for `/remove_time_spent`.
- Fix bad dependencies in Pipfile.


## 1.0.0 (2018-08-21)

Initial release
